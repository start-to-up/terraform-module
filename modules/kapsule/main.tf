provider "scaleway" {
  region = var.scaleway_region
  zone   = var.scaleway_zone
}

resource "scaleway_k8s_cluster" "main" {
  name        = var.scaleway_kapsule_cluster_name
  description = "cluster kapsule ${var.scaleway_kapsule_cluster_name}"
  version     = "1.19.4"
  cni         = var.scaleway_kapsule_cluster_cni
  tags        = ["terraform", var.scaleway_kapsule_cluster_name]
  project_id  = var.scaleway_kapsule_cluster_project_id
  autoscaler_config {
    disable_scale_down              = true
    scale_down_delay_after_add      = "10m"
    estimator                       = "binpacking"
    expander                        = "random"
    ignore_daemonsets_utilization   = true
    balance_similar_node_groups     = true
    expendable_pods_priority_cutoff = -5
  }
}

resource "scaleway_k8s_pool" "main" {
  cluster_id          = scaleway_k8s_cluster.main.id
  name                = var.scaleway_kapsule_cluster_name
  tags                = ["terraform", var.scaleway_kapsule_cluster_name]
  node_type           = var.scaleway_kapsule_cluster_node_type
  size                = var.scaleway_kapsule_cluster_pool_size
  autoscaling         = true
  autohealing         = true
  min_size            = var.scaleway_kapsule_cluster_pool_size_min
  max_size            = var.scaleway_kapsule_cluster_pool_size_max
  wait_for_pool_ready = true
}
