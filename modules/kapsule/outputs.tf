output "scaleway_kapsule_cluster_url" {
  description = "Url of kapsule cluster"
  value       = scaleway_k8s_cluster.main.apiserver_url
}

output "scaleway_kapsule_cluster_kubeconfig" {
  description = "Kubeconfig file of kapsule cluster"
  value       = scaleway_k8s_cluster.main.kubeconfig[0].config_file
}
