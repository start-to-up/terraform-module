# Terraform-module kapsule

Terraform module to install a kubernetes kapsule cluster from scaleway.

We use the modules provided in the hashicorp registry [scaleway_k8s_cluster](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/k8s_cluster) and [scaleway_k8s_pool](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/k8s_pool).

## Use

Add credential in terraform config file `.terraformrc` include in home user or create file in project folder and add `TF_CLI_CONFIG_FILE=.terraformrc` in `.envrc`.

```text
credentials "gitlab.com" {
  token = "xxxxx"
}
```

Include module block in terraform file.

```hcl
module "scaleway_k8s" {
  source = "gitlab.com/start-to-up/terraform-module/kapsule-local"
  version = "x.x.x"

  scaleway_region               = "fr-par"
  scaleway_zone                 = "fr-par-1"
  scaleway_kapsule_cluster_name = "demo"
  scaleway_kapsule_cluster_project_id = "6f46964b-xxxxx"
}

output "url" {
  value = module.scaleway_k8s.scaleway_kapsule_cluster_url
}

output "kubeconfig" {
  value = module.scaleway_k8s.scaleway_kapsule_cluster_kubeconfig
}
}
```

## Variables

| name                                   | description                                              | default |
| -------------------------------------- | -------------------------------------------------------- | ------- |
| scaleway_region                        | Scaleway region                                          |         |
| scaleway_zone                          | Scaleway zone                                            |         |
| scaleway_kapsule_cluster_name          | Name of kapsule cluster                                  |         |
| scaleway_kapsule_cluster_cni           | CNI of kapsule cluster                                   | cilium  |
| scaleway_kapsule_cluster_node_type     | Node instance type of kapsule cluster                    | DEV1-M  |
| scaleway_kapsule_cluster_pool_size     | Amount of worker in the pool of kapsule cluster          | 2       |
| scaleway_kapsule_cluster_pool_size_min | Minimum number of workers in the pool of kapsule cluster | 2       |
| scaleway_kapsule_cluster_pool_size_max | Maximum number of workers in the pool of kapsule cluster | 5       |
