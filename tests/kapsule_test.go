package test

import (
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/k8s"
	"github.com/gruntwork-io/terratest/modules/terraform"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func TestTerraformHelloWorldExample(t *testing.T) {
	print("oki\n")
	scalewayOrganisationID, scalewayOrganisationIDStatus := os.LookupEnv("TEST_SCW_DEFAULT_ORGANIZATION_ID")
	scalewayProjectID, scalewayProjectIDStatus := os.LookupEnv("TEST_SCW_PROJECT_ID")
	scalewayAccessKey, scalewayAccessKeyStatus := os.LookupEnv("TEST_SCW_ACCESS_KEY")
	scalewaySecretKey, scalewaySecretKeyStatus := os.LookupEnv("TEST_SCW_SECRET_KEY")

	os.Setenv("KUBECONFIG", ".kubeconfig")

	if !scalewayOrganisationIDStatus || !scalewayAccessKeyStatus || !scalewaySecretKeyStatus || !scalewayProjectIDStatus {
		print("Missing scaleway credential and organisation variables in environment.\n")
		os.Exit(1)
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../modules/kapsule",
		Vars: map[string]interface{}{
			"scaleway_region":                     "fr-par",
			"scaleway_zone":                       "fr-par-1",
			"scaleway_kapsule_cluster_name":       "demo",
			"scaleway_kapsule_cluster_project_id": scalewayProjectID,
		},
		EnvVars: map[string]string{
			"SCW_DEFAULT_ORGANIZATION_ID": scalewayOrganisationID,
			"SCW_ACCESS_KEY":              scalewayAccessKey,
			"SCW_SECRET_KEY":              scalewaySecretKey,
		},
	})

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	terraform.OutputRequired(t, terraformOptions, "scaleway_kapsule_cluster_url")
	kubeconfig := terraform.OutputRequired(t, terraformOptions, "scaleway_kapsule_cluster_kubeconfig")

	f, err := os.Create(".kubeconfig")
	check(err)
	defer os.Remove(".kubeconfig")
	f.WriteString(kubeconfig)

	k8sOptions := k8s.NewKubectlOptions("", "", "default")

	k8s.GetNodes(t, k8sOptions)
}
