# Test

We use the [terratest](https://terratest.gruntwork.io/) utility to run tests on terraform modules.

## Execute

To run a test, there is the `test` command in the Makefile file at the root of the project. Add the variable `TARGET` with the name of the module.

```bash
TARGET=kapsule make test
```

## Providers

### Scaleway

To use scaleway as provider, it is necessary to pass organization ID and that of a project, otherwise we receive an error. See [article](https://gitmemory.com/issue/scaleway/terraform-provider-scaleway/606/706429021)

```bash
Error: scaleway-sdk-go: insufficient permissions: 
```
