module terraform-module

go 1.16

require (
	github.com/gruntwork-io/terratest v0.36.8
	github.com/scaleway/scaleway-sdk-go v1.0.0-beta.7
)
