# terraform-module

This project contains terraform modules that can be used by anyone with access to the group.

## Use

See the `README.md` included in the module

## Lint

You can run the linter on a module using the `TARGET` variable with module name and the make `lint` command

```bash
TARGET=kapsule make lint
```

> You can also add the copy `.envrc.tpl` in `.envrc` file and change `TARGET` variable.

## Test

Same has Lint use make `test` command.

## Validate

Same has Lint use make `validate` command.
