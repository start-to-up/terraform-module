check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
        $(error Undefined $1$(if $2, ($2))$(if $(value @), \
                required by target `$@')))

lint:
	@:$(call check_defined, TARGET, Module name)
	terraform -chdir=./modules/$(TARGET) init
	terraform -chdir=./modules/$(TARGET) fmt -check=true
	terraform -chdir=./modules/$(TARGET) validate

validate:
	@:$(call check_defined, TARGET, Module name)
	docker container run --rm -v $(PWD)/modules/$(TARGET):/path checkmarx/kics:v1.3.5-alpine scan -p "/path" -o "/path/"

test:
	@:$(call check_defined, TARGET, Module name)
	go test -v -timeout 30m ./tests/$(TARGET)_test.go

.SILENT: lint
.SILENT: validate
.SILENT: test
